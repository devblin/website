export default {
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Grey Software | Democratizing Software Education!',

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: ' Grey Software is a not-for-profit on a mission to give people access to software they can trust, love, and learn from!' },
      {
        name: 'og:title',
        content: 'Grey Software | Democratizing Software Education'
      },
      {
        name: 'og:description',
        content: ' Grey Software is a not-for-profit on a mission to give people access to software they can trust, love, and learn from!'
      },
      {
        name: 'og:image',
        content: 'https://grey.software/preview.png'
      },
      {
        name: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        name: 'twitter:title',
        content: 'Grey Software | Democratizing Software Education!'
      },
      {
        name: 'twitter:description',
        content: ' Grey Software is a not-for-profit on a mission to give people access to software they can trust, love, and learn from!'
      },
      {
        name: 'twitter:image',
        content: 'https://grey.software/preview.png'
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com', crossorigin: true },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxt/content',
    '@nuxtjs/toast',
  ],

  toast: {
    position: 'top-center',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
}
