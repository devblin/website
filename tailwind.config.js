const defaultTheme = require('tailwindcss/defaultTheme')
const { getColors } = require('theme-colors')
const path = require('path')


module.exports = {
  mode: 'jit',
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        xs: { max: '599px' },
        sm: { min: '600px' },
        md: { min: '900px' },
        lg: { min: '1200px' },
        xl: { min: '1800px' },
      },
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        'section-title': '32px',
      },
      colors: {
        black: getColors('#181818'),
        altblack: getColors('#2d2d2d'),
        altwhite: getColors('#fefefe'),
        secondary: getColors('#7e7e7e'),
      },
      spacing: {
        "navbar-mobile": '9vh',
        "navbar-pc": '12vh',
        '26': '6.5rem',
        '88': '22rem',
        '112': '28rem',
        '120': '30rem',
        '128': '32rem'
      },
      height: {
        hero: '90vh',
      },
    },
    variants: {
      extend: {
        padding: ['hover'],
        width: ['hover']
      },
    },
  },
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      `components/**/*.{vue,js}`,
      `layouts/**/*.vue`,
      `pages/**/*.vue`,
      `plugins/**/*.{js,ts}`,
      `nuxt.config.{js,ts}`
    ]
  }
}